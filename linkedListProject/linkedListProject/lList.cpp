#include "stdafx.h"
#include "lList.h"
#include<iostream>


LList::LList()
{

	head = NULL;
	count = 0;

}

void LList::addNode(int value)
{

	LLNode * newNode = new LLNode(value);
	newNode->next = head;
	head = newNode;
	count++;
}

bool LList::removeNode(int val)
{

	bool success = false;
	LLNode * current;
	LLNode * runner;
	runner = current = head;

	// if linkedlist is empty, then do nothing
	if (isEmpty()) return false;

	// if first node is it
	if (runner->value == val) {
		// create temp storage for head you are trying to delete, and delete //
		LLNode * temp = runner;
		head = runner->next;
		runner = current = head;
		delete temp;
		success = true;
	}
	else { // traverse remainder of list
		runner = runner->next;
		while (runner != NULL) {
			// move the runner and the current together //
			if (runner->value == val) {
				// we've found it
				LLNode * tmp = runner;
				current->next = runner->next;
				delete tmp;
				success = true;
				break;
			}
			runner = runner->next;
			current = current->next;
		}
	}
	return false;
}

void LList::printNodes()
{
	LLNode * current = head;
	int i = 1;
	while (current != NULL) {
		// print data //
		std::cout << i++ << ": " << current->value << std::endl;
		// move current pointer
		current = current->next;
	}
}

bool LList::isEmpty() {
	
	return (count == 0 ? true : false);
}

void LList::addNodeToEnd(int val)
{

	LLNode * current = head;
	LLNode * newNode = new LLNode(val);

	// if empty, then simply assign it
	// add node to the end of the list //
	if (isEmpty()) head = newNode;
	else if (count == 1) head->next = newNode; // if only one node, then point to newNode

	//traverse to the last node
	while (current->next != NULL) {
		current = current->next;
	}
	current->next = newNode;

}

bool LList::addNodeAfterValue(int newValue, int locationValue)
{

	bool success = false;
	LLNode * current = head; // use the current pointer to treverse the list
	if (isEmpty()) return false;
	while (current != NULL) 
	{
		if (current->value == locationValue) {
			LLNode * newNode = new LLNode(newValue);
			newNode->next = current->next;
			current->next = newNode;
			success = true;
			break;
		}
		current = current->next;
	}
	return success;
}

LLNode * LList::getHeadNode() {
	
	return head;
}

bool LList::removeFirst() {

	bool success = false;
	if (head != NULL) {
		LLNode * tmp = head;
		head = head->next;
		delete tmp;
		success = true;
	}
	return success;
}


LList::~LList()
{
	// clear out the linked list
	LLNode * tmp = head;
	while (head != NULL) {
		tmp = head;
		head = head->next;
		delete tmp;
	}

}
