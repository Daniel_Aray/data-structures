#include "stdafx.h"
#include "lLStack.h"


LLStack::LLStack() {

	count = 0;
	LL = new LList(); // delete new LList
}


LLStack::~LLStack() {

}

void LLStack::push(int value) {
	
	count++;
	LL->addNode(value);
}

void LLStack::pop() {

	LLNode * tmp = LL->getHeadNode();
	if (tmp != NULL) {
		LL->removeFirst();
		count--;
	}
}

int LLStack::peek() {

	LLNode * tmp = LL->getHeadNode();
	if (tmp == NULL) {
		return -99;
	}
	else if(tmp != NULL) {
		return tmp->value;
	}
}

int LLStack::getLength() {

	return count;
}

void LLStack::printAll() {

	LL->printNodes();
}
