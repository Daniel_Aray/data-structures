// linkedListProject.cpp : Defines the entry point for the console application.
//

// adding line for git testing 

#include "stdafx.h"
#include "lList.h"
#include "lLNode.h"
#include "lLStack.h"
#include "lLQueue.h"
#include "binaryTree.h"
#include "bTNode.h"

void LinkedListTest();
void LLStackTest();
void LLQueueTest();


int main() {
	// LinkedListTest();
	// LLStackTest();
	// LLQueueTest();
	
	return 0;
}

void LinkedListTest(){
	LList * LL = new LList();
	LL->addNode(24);
	LL->addNode(23);
	LL->addNode(1);
	LL->addNode(51);
	LL->addNode(12);

	// Pre end add
	std::cout << "Pre-End Addition" << std::endl;
	LL->printNodes();

	// Post end add
	std::cout << "\nPost_End Addition" << std::endl;
	LL->addNodeToEnd(99);
	LL->printNodes();

	// Removal Test
	std::cout << "\nRemove 51" << std::endl;
	LL->removeNode(51);
	LL->printNodes();
	std::cout << "\nRemove 4, nothing should happen" << std::endl;
	LL->removeNode(4);
	LL->printNodes();
	std::cout << "\nRemove 12, nothing should happen" << std::endl;
	LL->removeNode(12);
	LL->printNodes();

	// Add to location Test
	std::cout << "\nAdd 99 after the value 23" << std::endl;
	LL->addNodeAfterValue(99, 23);
	LL->printNodes();

	std::cout << "\nRemove 99, nothing should happen" << std::endl;
	LL->removeNode(99);
	LL->printNodes();
	std::cout << "\nRemove 99, nothing should happen" << std::endl;
	LL->removeNode(99);
	LL->printNodes();

	system("pause");

}
void LLStackTest() {
	LLStack * LL = new LLStack();
	std::cout << "Before Removal" << std::endl;
	LL->push(5);
	LL->push(4);
	LL->push(3);
	LL->push(2);
	LL->push(1);

	std::cout << "\nAfter First Removal" << std::endl;
	LL->pop();
	LL->printAll();
	

	std::cout << "\nAfter Second Removal" << std::endl;
	LL->pop();
	LL->printAll();

	std::cout << "Peek First Node: " << LL->peek() << std::endl;
	LL->getLength();

	//adding node
	LL->push(2);

	std::cout << " After Add: " << std::endl;
	LL->printAll();

	system("pause");
 }

void LLQueueTest() {
	LLQueue * LL = new LLQueue();
	LL->enqueue(1);
	LL->enqueue(2);
	LL->enqueue(3);
	LL->enqueue(4);
	LL->enqueue(5);
	std::cout << "Before Removal" << std::endl;
	LL->printAll();
	

	std::cout << "Peek First Node: \n" << LL->peek() << std::endl;

	std::cout << "adding node\n " << std::endl;
	LL->enqueue(6);
	LL->printAll();

	std::cout << "Removing Node: " << std::endl;
	LL->dequeue();
	LL->printAll();

	std::cout << "get length: " << std::endl;
	LL->getLength();
	LL->printAll();

	system("pause");

	
}
// create basic test function 