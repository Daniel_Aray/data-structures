#pragma once

class BTNode {
public:
	int value;

	BTNode * parent;
	BTNode * leftChild;
	BTNode * rightChild;

	BTNode(int val);
	
	BTNode();
	~BTNode();
};

