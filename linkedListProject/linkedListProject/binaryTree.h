#pragma once
#include "bTNode.h"
class BinaryTree {
private:
	BTNode * root;
	int count;


public:
	BinaryTree();
	~BinaryTree();

	void addNode(int value);
	void removeNode(int value);
	void printAll();
};

