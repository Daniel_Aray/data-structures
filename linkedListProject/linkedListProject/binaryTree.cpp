#include "stdafx.h"
#include "binaryTree.h"


BinaryTree::BinaryTree() {
	root = NULL;
	count = 0;
}


BinaryTree::~BinaryTree() {
}

void BinaryTree::addNode(int value) {

	BTNode * newNode = new BTNode(value);

	if (root == NULL) {
		root = newNode;
	}

	else {
		// runner to find place of insertion
		BTNode * currentNode = root;

		while (currentNode != NULL) {
			if (newNode->value < currentNode->value) {
				if (currentNode->leftChild == NULL) {
					currentNode->leftChild = newNode;
					newNode->parent = currentNode;
					break;
				}
				else {
					currentNode = currentNode->leftChild;
				}
			}
			else {
				if (currentNode->rightChild == NULL) {
					currentNode->rightChild = newNode;
					newNode->parent = currentNode;
					break;
				}
				else {
					currentNode = currentNode->rightChild;
				}
			}
		}

	}
	count++;
}

void BinaryTree::removeNode(int value) {
}

void BinaryTree::printAll() {
}
