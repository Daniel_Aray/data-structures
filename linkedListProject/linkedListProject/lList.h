#pragma once
#include "lLNode.h"
#include <iostream>


/*struct Node {

public:
	Node * next;
	int value;

	Node(int val) {
		next = NULL;
		value = val;
	}

};*/

class LList
{
private:
	LLNode * head;
	int count;

public:
	LList();
	void addNode(int value); 
	bool removeNode(int val); 
	void printNodes();
	bool isEmpty();
	void addNodeToEnd(int val);
	bool addNodeAfterValue(int newValue, int locationValue);
	LLNode * getHeadNode();
	bool removeFirst();

	~LList();
};

