#pragma once
#include "lList.h"
#include "lLNode.h"

class LLStack {

 public:
	LLStack();
	~LLStack();

	void	push(int value);
	void	pop();
	int		peek();
	int		getLength();
	void	printAll();

 private:
	 int count;
	 LList * LL;
};

