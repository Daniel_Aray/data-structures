#include "stdafx.h"
#include "lLQueue.h"


LLQueue::LLQueue() {

	count = 0;
	LL = new LList();
}


LLQueue::~LLQueue() {

}

void LLQueue::enqueue(int value) {

	count++;
	LL->addNode(value);
}

void LLQueue::dequeue() {
	LLNode * tmp = LL->getHeadNode();
	if (tmp != NULL) {
		LL->removeFirst();
		count--;
	}
}

int LLQueue::peek() {

	LLNode * tmp = LL->getHeadNode();
	if (tmp == NULL) {
		return -99;
	}
	else if (tmp != NULL) {
		return tmp->value;
	}
}

int LLQueue::getLength() {

	return count;
}

void LLQueue::printAll() {

	LL->printNodes();
}
