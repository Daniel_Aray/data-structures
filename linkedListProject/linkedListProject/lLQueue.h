#pragma once
#include"lList.h"
#include"lLNode.h"
class LLQueue {


public:
	LLQueue();
	~LLQueue();

	void	enqueue(int value);
	void	dequeue();
	int		peek();
	int		getLength();
	void	printAll();

private:
	int count;
	LList * LL;
};

